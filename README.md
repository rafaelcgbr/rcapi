# RentalCars Front-end Developer Test

This is a test for a position on RentalCars.com as Front-end Developer.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Deployment

Although there's a couple of different forms of running the project, I'd encourage you to run it by:

-Travelling to the /build folder
-Use the command "serve -s build"
-Navigation to the URL presented in the Terminal

-To install serve: npm install -g serve


## Built With

* [ReactJS](https://reactjs.org/)

## Author

* **Rafael Cammarano Guglielmi** - rafaelcg@gmail.com
