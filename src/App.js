import React, { Component } from 'react';

import Header from './components/Header'
import PopularCities from './components/PopularCities'

class App extends Component {
    
  componentWillMount() {
    document.title = 'Cheap Car Hire, Compare Rental Prices - Rentalcars.com'
  }
    
  render() {
    return (
        <div>
            <Header />
            <PopularCities />
        </div>
    );
  }
}

export default App;