import fetch from 'isomorphic-fetch';

// Polyfill Promises for IE and older browsers.
require('es6-promise').polyfill();

const SEARCH_URI = 'https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do';

export default function GetCities(query) {
    
  return fetch(`${SEARCH_URI}?solrIndex=fts_en&solrRows=6&solrTerm=${query}`)
    .then((resp) => resp.json())
    .then(({results}) => {
      const options = results.docs.map((i) => ({
        //id: i.placeKey,
        country: i.country + '',
        name: i.name + '',
        type: i.placeType + '',
        city: i.city + '',
        region: i.region + '',
        iata: i.iata + '',
        alternative: i.alternative + ''
      }));
      return {options};
    });
}