import React from 'react';
import PropTypes from 'prop-types';

import {Highlighter} from 'react-bootstrap-typeahead';

class CitiesMenuItem extends React.Component {
    constructor (props) {
        super(props);
        
    }
    
    getBadgeByType = () => {
        
        const typeSymbol = this.props.place.type;
        
        var typeName = "";
        var className = "";
        
        if(typeSymbol === "A"){
            typeName = "Airport";
            className = "badge-red";
        }else if(typeSymbol === "C"){
            typeName = "City";
            className = "badge-blue";
        }else if(typeSymbol === "T"){
            typeName = "Station";
            className = "badge-gray";
        }else if(typeSymbol === "D"){
            typeName = "District";
            className = "badge-green";
        }else {
            typeName = "Other";
            className = "badge-gray";
        }
        
        return <span className={`badge ${className}`}>{typeName}</span>;
    }
    
    getLocation = () => {
        
        var locationString = this.props.place.country;
        
        if(this.props.place.city != "undefined"){
            locationString = this.props.place.city + ', ' + locationString; 
        }else if (this.props.place.region != "undefined") {
            locationString = this.props.place.region + ', ' + locationString;
        }
        
        return locationString;
    }
    
    getName = () => {
        var searchQuery = this.props.props.text.toString().toLowerCase();
        var currentName = this.props.place.name.toString().toLowerCase();
        var searchRegex = new RegExp(searchQuery, "gi");
        var iata = "";
        
        if(this.props.place.type === 'A'){
            iata = " (" + this.props.place.iata + ")";
        }
        
        if(currentName.indexOf(searchQuery) === 0){
            
            currentName = currentName.replace(searchRegex, '');
            //console.log(searchQuery);
            searchQuery = searchQuery.charAt(0).toUpperCase() + searchQuery.slice(1);
            return (<div><strong>{searchQuery}</strong>{currentName} {iata}</div>);
        }else {
            return (<span>{currentName} {iata}</span>);
        }
        
        
    }
    
    render (){
        return(
            <div>
                <div className='containerBadgeLeft'>
                    {this.getBadgeByType()}
                </div>
                <div className='containerInfoRight'>
                    <span>{this.getName()}</span>
                    <div className='subtitleLocation'>
                        <small>{this.getLocation()}</small>
                    </div>
                </div>
            </div>
        );
    }
}

CitiesMenuItem.propTypes = {
  place: PropTypes.shape({
    type: PropTypes.string.isRequired,
  }),
};

export default CitiesMenuItem;