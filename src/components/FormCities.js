import React, {Fragment} from 'react';

import 'react-bootstrap-typeahead/css/Typeahead.css';
import {AsyncTypeahead} from 'react-bootstrap-typeahead';

import CitiesMenuItem from './CitiesMenuItem';
import GetCities from './GetCities';

class FormCities extends React.Component {
    
    state = {
        allowNew: false,
        isLoading: false,
        multiple: false,
        options: [],
      };

    _handleSearch = (query) => {
        this.setState({isLoading: true});
        GetCities(query)
          .then(({options}) => {
            this.setState({
              isLoading: false,
              options,
            }, console.log(options));
          });
    }
    
    render () {
        
        return (
          <Fragment>
            
            <AsyncTypeahead
                {...this.state}
                minLength={2}
                labelKey="name"
                filterBy={['region', 'city', 'country', 'alternative']}
                onSearch={this._handleSearch}
                placeholder = "city, airport, station, region, district..."
                renderMenuItemChildren={(option, props) => (
                    <CitiesMenuItem place={option} props={props} />
                )}
                
            />
          </Fragment>
        );
    }
}

export default FormCities;