import React from 'react';

import FormCities from './FormCities';
import Navigation from './Navbar'
import BrandsList from './BrandsList'

import { Jumbotron, Grid, Row, Col, Clearfix, Checkbox } from 'react-bootstrap';

class Header extends React.Component {

    constructor (props) {
        super(props);
        
        this.state = {
            mainTitle: "Car Hire – Search, Compare & Save",
            subMainTitle: "Compare 900 companies at over 53,000 locations. Best price guaranteed"
        }
    }

    render () {
        return (
            <div>
                <Navigation />
                <Jumbotron>
                    <Grid>
                        <div className="headerTitles">
                            <h1 className="text-center">{this.state.mainTitle}</h1>
                            <p className="text-center">{this.state.subMainTitle}</p>
                        </div>
                        <Row>
                            <Col xs={12} lg={6}>
                                <div className="yellowContainer">
                                    <h2>Where are you going?</h2>
                                    <FormCities />
                                </div>
                            </Col>
                            <Col xs={12} lg={6}>
                                <div className="whiteContainer">
                                    <p>Rentalcars.com connects you to the biggest brands in car hire.</p>
                                    <BrandsList />
                                </div>
                            </Col>
                            <Clearfix />
                        </Row>
                    </Grid>
                </Jumbotron>
            </div>
        );
    }
}

export default Header;