import React from 'react';

import { Jumbotron, Grid, Row, Col } from 'react-bootstrap';

class BrandsList extends React.Component {
    render () {
        return (
            <header>
                <div><img src="https://cdn.rcstatic.com/images/supplier_logos/hertz_logo_lrg.gif" /></div>
                <div><img src="https://cdn.rcstatic.com/images/supplier_logos/avis_logo_lrg.gif" /></div>
                <div><img src="https://cdn.rcstatic.com/images/supplier_logos/europcar_logo_lrg.gif" /></div>
                <div><img src="https://cdn.rcstatic.com/images/supplier_logos/alamo_logo_lrg.gif" /></div>
                <div><img src="https://cdn.rcstatic.com/images/supplier_logos/budget_logo_lrg.gif" /></div>
                <div><img src="https://cdn.rcstatic.com/images/supplier_logos/enterprise_logo_lrg.gif" /></div>
                <div><img src="https://cdn.rcstatic.com/images/supplier_logos/dollar_logo_lrg.gif" /></div>
                <div><img src="https://cdn.rcstatic.com/images/supplier_logos/thrifty_logo_lrg.gif" /></div>
            </header>
        );
    }
}

export default BrandsList;