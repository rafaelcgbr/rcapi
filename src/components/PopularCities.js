import React from 'react';

import { Row, Col, Grid } from 'react-bootstrap';

import ImagePopularCity from './ImagePopularCity';

class PopularCities extends React.Component {
    render () {
        return (
            <Grid>
                <h3>Popular Destinations</h3>
                <Row>
                    <Col xs={12} md={6}>
                        <ImagePopularCity target="amalfi" title="Amalfi Coast" country="Italy" />
                    </Col>
                    <Col xs={12} md={6}>
                        <ImagePopularCity target="iceland" title="reykjavik" country="Iceland" />
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} md={6}>
                        <ImagePopularCity target="rio" title="Rio de Janeiro" country="Brazil" />
                    </Col>
                    <Col xs={12} md={6}>
                        <ImagePopularCity target="newyork" title="New York" country="USA" />
                    </Col>
                </Row>
            </Grid>
            
        );
    }
}

export default PopularCities;