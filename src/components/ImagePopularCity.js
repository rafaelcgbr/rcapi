import React from 'react';

import { Button } from 'react-bootstrap';

class ImagePopularCity extends React.Component {
    
    constructor (props) {
        super(props);
        
        this.state = {
            imageSrc: '',
        }
    }
    
    render() {
        return (
            <div className="containerPopularDestination">
                <img className="imagePopularDestination" src={require('../images/destinations/' + this.props.target + '.jpg')} />
                <div className="overlayPopularDestination">
                    <p>{this.props.title}</p>
                    <p>{this.props.country}</p>
                </div>
            </div>
        )
    }
}

export default ImagePopularCity;