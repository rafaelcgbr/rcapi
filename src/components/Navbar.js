import React from 'react';

import { Navbar, Nav, NavItem } from 'react-bootstrap';

class Navigation extends React.Component {
    render (){
        return (
            <Navbar fixedTop>
              <Navbar.Header>
                <Navbar.Brand>
                  <a href="#brand"><img className='logoRC' src="https://cdn.rcstatic.com/images/rclogo/blue/2x/wordmark-rclogo_194x30.png" alt="RentalCars.com" /></a>
                </Navbar.Brand>
              </Navbar.Header>
            </Navbar>
        )
    }
}

export default Navigation;